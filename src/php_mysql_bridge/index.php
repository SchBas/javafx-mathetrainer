<?php

//ini_set('display_errors', '1');
//ini_set('display_startup_errors', '1');
//error_reporting(E_ALL);

require_once 'datenbank.inc.php';
require_once 'Abfrage.php';

$action = $_GET['action'];
$username = $_GET['username'];
$password = $_GET['password'];
$userid = $_GET['userid'];
$mode = $_GET['mode'];
$totalanswers = $_GET['totalanswers'];
$rightanswers = $_GET['rightanswers'];
$time = $_GET['time'];
$points = $_GET['points'];


if ($action == 'getuserid') {

    $sql = vsprintf(
        'SELECT userid FROM user WHERE username = \'%s\' AND password = \'%s\'',
        array($username, $password)
    );

    echo Abfrage::executeFetch($sql)['userid'];

} elseif ($action == 'validatelogin') {

    $sql = vsprintf(
        'SELECT count(*) FROM user WHERE username = \'%s\' AND password = \'%s\'',
        array($username, $password)
    );

    echo Abfrage::executeFetch($sql)['count(*)'];

} elseif ($action == 'registeruser') {

    $sql = vsprintf(
        'INSERT INTO user(username, password) VALUES (\'%s\', \'%s\')',
        array($username, $password)
    );

    Abfrage::simpleExecute($sql);

} elseif ($action == 'addscore') {

    $sql = vsprintf(
        'INSERT INTO scores(userid, mode, totalanswers, rightanswers, time, points) VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\')',
        array($userid, $mode, $totalanswers, $rightanswers, $time, $points)
    );

    Abfrage::simpleExecute($sql);

} elseif ($action == 'gethighscores') {

    $sql = vsprintf(
        'SELECT user.username, scores.* FROM scores INNER JOIN user ON scores.userid=user.userid WHERE scores.mode = \'%s\' AND scores.userid = \'%s\''
        .' ORDER BY points DESC LIMIT 20', 
        array($mode, $userid)
    );

    Abfrage::printScores($sql);

} elseif ($action == 'getbestenliste') {

    $sql = vsprintf(
        'SELECT user.username, scores.* FROM scores INNER JOIN user ON scores.userid=user.userid WHERE scores.mode = \'%s\' ORDER BY points DESC LIMIT 20',
        array($mode)
    );
    Abfrage::printScores($sql);
}