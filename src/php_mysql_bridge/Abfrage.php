<?php

require_once 'datenbank.inc.php';

class Abfrage {

    public static function simpleExecute($sql) {
        global $db;
        $abfrage = $db->prepare($sql);
        $abfrage->execute();
        return $abfrage;
    }

    public static function executeFetch($sql) {
        return self::simpleExecute($sql)->fetch();
    }

    public static function executeFetchAll($sql) {
        return self::simpleExecute($sql)->fetchAll();
    }

    public static function printScores($sql) {
        $resultArray = self::executeFetchAll($sql);
        foreach ($resultArray as $score) {
            echo $score['username']." ".$score['mode']." ".$score['totalanswers']." ".$score['rightanswers']." ".$score['time']." ".$score['points']." ";
        }
    }
}