<?php

// Entwicklung PDO::ERRMODE_EXEPTION und live PDO::ERRMODE_SILENT
$option = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
];

die; //Auskommentieren sobald unten alle platzhalter ersetzt wurden

// evtl. Verbindung zur Datenbank (Funktioniert nur, wenn Sie die Richtigen Daten herausfinden)
$db = new PDO(
    'mysql:host=hostplaceholder;dbname=databaseplaceholder', // neue DB
    'dbuserplaceholder',
    'passwordplaceholder',
    $option
);

$db->query('SET NAMES utf8');
