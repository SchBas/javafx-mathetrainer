package sample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


//VERBINDUNGSVERSUCH VON HASAN

//WICHTIG: https://www.youtube.com/watch?v=DH3dWzmkT5Y

public class ConnectionClass {
    public Connection databaseLink;

    public Connection getConnection() {
        String hostname = "192.168.122.215";
        String port = "3306";
        String dbname = "mathetrainerdb";
        String user = "adminer";
        String password = "Test1234";
        String url = "jdbc:mysql://" + hostname + ":" + port + "/" + dbname + "?maxAllowedPacket=5435456";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            databaseLink = DriverManager.getConnection(url, user, password);

        } catch (Exception e) {
            System.out.println("failed to connect in connectionClass");
            System.err.println("Unable to load driver.");
            e.printStackTrace();
            e.getCause();
        }
        return databaseLink;
    }
}