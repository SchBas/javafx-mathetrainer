
package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Arrays;

public class Eval extends Application {

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) {
        TextField textField = new TextField("");//Für die Benutzereingaben
        Text text = new Text("");//Für die Ausgaben
        GridPane root = new GridPane();//Für die Darstellung
        root.add(textField, 0, 0);
        root.add(text, 0, 1);
        textField.setOnKeyReleased(value -> {
            if (value.getCode().equals(KeyCode.ENTER)) eval(textField.getText(), text);
        });
        stage.setScene(new Scene(root, 200, 230));
        stage.show();
    }

    private static void eval(String aufgabe, Text text) {
        text.setText("Wird berechnet");//Ladestufe 1, ganz am Anfang
        char[] operanten = new char[aufgabe.length()];//Alle Operanten werden gespeichert. Möglich: +, -, *, /. Eventuell auch: %, ()
        for (int i = 0; i < aufgabe.length(); i++) {
            if (aufgabe.charAt(i) == '+' || (aufgabe.charAt(i) == '-' && i > 0 && (aufgabe.charAt(i - 1) != '(')) || aufgabe.charAt(i) == '*' || aufgabe.charAt(i) == '/')//Minus ist doppeldeutig: Es könnte auch eine Negative Zahl sein und nicht der Operator
                operanten[i] = aufgabe.charAt(i);
        }
        if (String.valueOf(operanten[0]).equals(""))
            text.setText("Keine Aufgabe eingegeben!");//Ohne Operanten keine Aufgabe
        int[] zahlen = new int[aufgabe.length()];//Speicherplatz aller Zahlen
        Arrays.fill(zahlen, Integer.MIN_VALUE);
        for (int i = 0; i < aufgabe.length(); i++) {
            if (aufgabe.charAt(i) == '(') {//Alle Zahlen müssen in Klammern sein, damit es einfacher ist
                int endzahl = aufgabe.indexOf(')', i);
                if(prufInt(aufgabe.substring(i+1, endzahl), text)) zahlen[i] = Integer.parseInt(aufgabe.substring(i+1, endzahl));
            }
        }
        String aufgabezusamm = zahlen[0] + "";
        for(int i = 1; i<zahlen.length; i++){
            if(zahlen[i] != Integer.MIN_VALUE ){
                aufgabezusamm += ""+operanten[i-1] +""+ zahlen[i]+"";
            }
        }
        int[] relevantzahl = new int[zahlen.length];
        for(int i = 0; i<zahlen.length; i++){
            for(int ii = 0; ii<zahlen.length; ii++) {
                if (zahlen[ii] != Integer.MIN_VALUE) {
                    relevantzahl[i] = zahlen[ii];
                    i++;
                    zahlen[ii] = Integer.MIN_VALUE;
                    //ii=Integer.MAX_VALUE;
                }
            }
        }
        int[] relevantoper = new int[zahlen.length];
        for(int i = 0; i<relevantoper.length; i++){
            for(int ii = 0; ii<zahlen.length; ii++){
                if(operanten[ii]!=0 && operanten[ii] != 'ö'){
                    relevantoper[i] = operanten[ii];
                    i++;
                    operanten[ii] = 'ö';
                }
            }
        }
        for(int i = 0; i<relevantzahl.length; i++){
            System.out.println("Relevantzahl["+i+"]: "+relevantzahl[i]);
            System.out.println("Relevantoper["+i+"]: "+(char)relevantoper[i]);
        }
        int[] ergebnisse = new int[aufgabe.length()];
        int ergebnis = 0;
        for (int i = 0; i<zahlen.length; i++) {
            if ((char) relevantoper[i] == '*') {
                System.out.println(ergebnis+"*"+relevantzahl[i+1]);
                ergebnis+= relevantzahl[i]*relevantzahl[i+1];
            } else if ((char) relevantoper[i] == '/') {
                System.out.println(ergebnis+"/"+relevantzahl[i+1]);
                ergebnis += relevantzahl[i]/relevantzahl[i + 1];
            }
        }
        for (int i = 0; i<relevantzahl.length; i++) {
            if (ergebnisse[i] == 0) {
                if ((char) relevantoper[i] == '+') {
                    System.out.println(ergebnis+" + "+ relevantzahl[i]+"+"+relevantzahl[i+1]);
                    ergebnis+= relevantzahl[i]+relevantzahl[i+1];
                } else if ((char) relevantoper[i] == '-') {
                    System.out.println(ergebnis+" - "+ relevantzahl[i+1]);
                    ergebnis +=  /*relevantzahl[i]*/ - relevantzahl[i+1];
                }
            }
        }
        /*for(int i = 0; i<ergebnisse.length; i++){
            System.out.println("Ergebnisse["+i+"]: "+ergebnisse[i]);
            ergebnis+=ergebnisse[i];
        }*/
        text.setText(text.getText()+"\n"+aufgabezusamm+" = "+ergebnis);
        /*text.setText("Wird berechnet .");//Ladestufe 2
        text.setText("Wird berechnet ..");//Ladestufe 3
        text.setText("Wird berechnet ...");//Ladestufe 4
        text.setText("Wurde berechnet! Ergebnis: ");//Ganz am Ende*/
    }

    private static boolean prufInt(String string, Text text) {
        try {
            Integer.parseInt(string);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}