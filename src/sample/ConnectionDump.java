package sample;

import java.sql.Connection;
import java.sql.Statement;

public class ConnectionDump {

    /*

    ConnectionClass connectNow = new ConnectionClass();
        Connection connectDB = connectNow.getConnection();

        String getUserId = "SELECT userid FROM user WHERE username = '" + username + "' AND password = '" + password + "';";

        try {
            Statement statement = connectDB.createStatement();
            ResultSet queryResult = statement.executeQuery(getUserId);

            queryResult.next();
            userid = queryResult.getInt(1);

            System.out.println("userid: " + userid);
        } catch (Exception e) {
            System.out.println("failed to get ID");
            e.printStackTrace();
            e.getCause();
        }


    public boolean validateLogin(String name, String password) {
        ConnectionClass connectNow = new ConnectionClass();
        Connection connectDB = connectNow.getConnection();

        String verifyLogin = "SELECT count(*) FROM user WHERE username = '" + name + "' AND password = '" + password + "'";

        try {

            Statement statement = connectDB.createStatement();
            ResultSet queryResult = statement.executeQuery(verifyLogin);

            //while (queryResult.next()) {
            queryResult.next();
                if (queryResult.getInt(1) == 1) {
                    System.out.println("user exists");
                    return true;
                }
            //}
            System.out.println("user does not exist");

        } catch (Exception e) {
            System.out.println("failed to connect in validateLogin");
            e.printStackTrace();
            e.getCause();
        }

        return false;
    }


    public boolean registerUser(String username, String password) {
        if (!validateLogin(username, password)) {
            ConnectionClass connectNow = new ConnectionClass();
            Connection connectDB = connectNow.getConnection();

            String insertIntoRegister = "INSERT INTO user(username, password) VALUES ('" + username + "','" + password + "')";

            try {

                Statement statement = connectDB.createStatement();
                statement.executeUpdate(insertIntoRegister);

                System.out.println("user registered");
                return true;

            } catch (Exception e) {
                System.out.println("failed to connect");
                e.printStackTrace();
                e.getCause();
            }
        }
        return false;
    }

    public boolean addScoreToDB() {
        ScoreSet newSet = currentMode.getScoreSet();

        ConnectionClass connectNow = new ConnectionClass();
        Connection connectDB = connectNow.getConnection();

        String insertInto = "INSERT INTO scores(userid, mode, totalanswers, rightanswers, time, points) VALUES ('"
                + user.userid + "','" + newSet.mode + "','" + newSet.totalAnswers + "','"
                + newSet.rightAnswers + "','" + newSet.time + "','" + newSet.points + "')";

        try {

            Statement statement = connectDB.createStatement();
            statement.executeUpdate(insertInto);

            System.out.println("score saved");
            return true;

        } catch (Exception e) {
            System.out.println("failed to connect");
            e.printStackTrace();
            e.getCause();
        }
        return false;
    }

    public ScoreSet[] getHighScoreArray() {
        ScoreSet[] highScoreArray = new ScoreSet[20];
        if (user == null) {

            int pointsBelow = Integer.MAX_VALUE;
            int arrayItemCount = 0;
            for (ScoreSet arrayItem : offlineScoreArray) if (arrayItem != null && arrayItemCount <= 20) arrayItemCount++;

            for (int highI = 0; highI < arrayItemCount; highI++) {
                int currentMaxPoints = 0;
                int targetI = 0;
                for (int arrayI = 0; arrayI < arrayItemCount; arrayI++) {
                    if (offlineScoreArray[arrayI].mode == getCurrentMode().getModeNum() && offlineScoreArray[arrayI].points > currentMaxPoints
                            && offlineScoreArray[arrayI].points < pointsBelow) {
                        targetI = arrayI;
                        currentMaxPoints = offlineScoreArray[arrayI].points;
                    }
                }
                highScoreArray[highI] = offlineScoreArray[targetI];
                pointsBelow = highScoreArray[highI].points;
            }
        } else {

            ConnectionClass connectNow = new ConnectionClass();
            Connection connectDB = connectNow.getConnection();

            String username;
            int mode, totalanswers, rightanswers, time, points;
            //String getHighScores =
            // "SELECT * FROM scores WHERE mode = '" + getCurrentMode().getModeNum() + "' AND userid = '" + user.userid +
            // "' ORDER BY points DESC LIMIT 20";
            String getHighScores = "SELECT user.username, scores.* FROM scores INNER JOIN user ON scores.userid=user.userid WHERE scores.mode = '"
                    + getCurrentMode().getModeNum() + "' AND scores.userid = '" + user.userid + "' ORDER BY points DESC LIMIT 20";

            try {

                Statement statement = connectDB.createStatement();
                ResultSet queryResult = statement.executeQuery(getHighScores);
                System.out.println("highscore query successful");

                int highI = 0;
                while (queryResult.next()) {
                    username = queryResult.getString(1);
                    mode = getCurrentMode().getModeNum();
                    totalanswers = queryResult.getInt(4);
                    rightanswers = queryResult.getInt(5);
                    time = queryResult.getInt(6);
                    points = queryResult.getInt(7);

                    //String username = user.username;

                    highScoreArray[highI] = new ScoreSet(username, mode, totalanswers, rightanswers, time, points);
                    highI++;
                }
                System.out.println("highscore extraction successful");

            } catch (Exception e) {
                System.out.println("failed to connect in getHighScoreArray");
                e.printStackTrace();
                e.getCause();
            }
        }

        return highScoreArray;
    }

public ScoreSet[] getBestenlisteArray() {
        ScoreSet[] bestenlisteArray = new ScoreSet[20];

        ConnectionClass connectNow = new ConnectionClass();
        Connection connectDB = connectNow.getConnection();

        String username;
        int mode, totalanswers, rightanswers, time, points;
        String getBestScores = "SELECT user.username, scores.* FROM scores INNER JOIN user ON scores.userid=user.userid WHERE scores.mode = '"
                + getCurrentMode().getModeNum() + "' ORDER BY points DESC LIMIT 20";

        try {

            Statement statement = connectDB.createStatement();
            ResultSet queryResult = statement.executeQuery(getBestScores);

            int bestI = 0;
            while (queryResult.next()) {
                username = queryResult.getString(1);
                mode = getCurrentMode().getModeNum();
                totalanswers = queryResult.getInt(4);
                rightanswers = queryResult.getInt(5);
                time = queryResult.getInt(6);
                points = queryResult.getInt(7);

                bestenlisteArray[bestI] = new ScoreSet(username, mode, totalanswers, rightanswers, time, points);
                bestI++;
            }

            System.out.println("Bestenliste query successful");

        } catch (Exception e) {
            System.out.println("failed to connect in getBestenlisteArray");
            e.printStackTrace();
            e.getCause();
        }

        return bestenlisteArray;
    }

     */
}
