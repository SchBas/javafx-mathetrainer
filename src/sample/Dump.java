package sample;

public class Dump {


}



/*
    Java Quellcode	SimpleQuery.java
        import java.sql.*;
 */
// Notice, do not import org.gjt.mm.mysql.*
// or you will have problems!

/*
public class SimpleQuery {

    public static void main(String[] args) {

        // Diese Eintraege werden zum
        // Verbindungsaufbau benoetigt.
        final String hostname = "localhost";
        final String port = "3306";
        final String dbname = "projekt";
        final String user = "frommer";
        final String password = "";

        Connection conn = null;

        try {
            System.out.println("* Treiber laden");
            Class.forName("org.gjt.mm.mysql.Driver").newInstance();
        }
        catch (Exception e) {
            System.err.println("Unable to load driver.");
            e.printStackTrace();
        }
        try {
            System.out.println("* Verbindung aufbauen");
            String url = "jdbc:mysql://"+hostname+":"+port+"/"+dbname;
            conn = DriverManager.getConnection(url, user, password);

            System.out.println("* Datenbank-Verbindung beenden");
            conn.close();
        }
        catch (SQLException sqle) {
            System.out.println("SQLException: " + sqle.getMessage());
            System.out.println("SQLState: " + sqle.getSQLState());
            System.out.println("VendorError: " + sqle.getErrorCode());
            sqle.printStackTrace();
        }

    } // ende: public static void main()

} // ende: public class SimpleQuery

 */

/*
Quelltextanalyse
        Mit der Klasse Connection können wir nun eine Verbindung zur Datenbank erstellen. Eine Instanz dieser Klasse wird erzeugt durch Anwendung der statischen Methode
        getConnection() der Klasse DriverManager. Durch die Anweisung
        conn = DriverManager.getConnection(url, user, password);
        werden folgende Schritte durchgeführt:
        Unter den geladenen Treibern wird ein Passender ausgesucht. Wird keiner gefunden, wird die Ausnahme SQLException geworfen.
        Danach wird versucht, eine Verbindung zu im String url spezifizierten Datenbank aufzubauen. Ferner wird geprüft, ob der Benutzer user mit dem Passwort password
        zugangsberechtigt ist.
        Schlägt der Aufbau der Verbindung fehl, wird eine SQLException geworfen.
        Die Spezifikation von url für die Verbindung zu einer Datenbank eröffnet mit jdbc, gefolgt vom Protokoll (hier mysql). Danach sind der Hostname
        (= Name des Rechners, auf dem der Datenbankserver läuft) und die Portnummer anzugeben. In diesem konkreten Fall wählen wir localhost und 3306 als Portnummer,
        da sowohl der Datenbankserver als auch die Java-Application SimpleQuery auf demselben Rechner laufen sollen. Konkret soll mit der Datenbank projekt gearbeitet werden.
        Mit der Anweisung conn.close() wird die Verbindung zur Datenbank wieder geschlossen.
        Die oben angegebene Java-Application kann als Rahmenprogramm verwendet werden, um darauf aufbauend Datenbank-Anwendungen zu erstellen. Die Hauptarbeit besteht dabei in der
        Auswahl eines geeigneten Treibers sowie der korrekten Konfiguration der Datenbank-Verbindung...
 */



/*
if (textField.getText().isBlank() == false && passwordField.getText().isBlank() == false) {
                ConnectionClass connectnow = new ConnectionClass();
                Connection connectDB = connectnow.getConnection();

                String verifyLogin = "SELECT count(1) FROM Benutzer WHERE Benutzername = '" + textField.getText() + "' AND Passwort = '" + passwordField.getText() + "'";

                try {
                    Statement stamement = connectDB.createStatement();
                    ResultSet queryResult = stamement.executeQuery(verifyLogin);

                    while (queryResult.next()) {
                        if (queryResult.getInt(1) == 1) {
                            Parent root2 = FXMLLoader.load(getClass().getResource("../files/hauptseite.fxml"));
                            Scene neueFenster2 = new Scene(root2);

                            Stage window2 = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
                            window2.setScene(neueFenster2);
                            window2.show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    e.getCause();
                }

 */