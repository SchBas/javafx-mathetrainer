package control;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import model.Main;
import model.Mathetrainer;

public class MainMenuController implements ControllerInterface {

    @FXML
    public Label loginTag;
    public Button loginButton;

    Mathetrainer mathetrainer;

    public void goToLogin(ActionEvent actionEvent) {
        if (!mathetrainer.loggedIn) {
            Main.primaryStage.setScene(Main.loginScene);
        } else {
            Main.subStage.setScene(Main.confirmLogoutScene);
            Main.subStage.showAndWait();
        }
    }

    public void goToChooseMode(ActionEvent actionEvent) {
        Main.primaryStage.setScene(Main.chooseModeScene);
    }

    public void exitApplication(ActionEvent actionEvent) {
        Main.subStage.setScene(Main.confirmExitScene);
        Main.subStage.showAndWait();
    }

    public void setMathetrainer(Mathetrainer giveMathetrainer) {
        mathetrainer = giveMathetrainer;
        if (loginTag != null) loginTag.setText("");

        mathetrainer.mainMenuUpdate.addListener(((observableValue, aBoolean, t1) -> {
            if (mathetrainer.isMainMenuUpdate()) {
                if (mathetrainer.loggedIn) {
                    loginButton.setText("Logout");
                    loginTag.setText("Logged in as: " + mathetrainer.user.username);
                } else {
                    loginButton.setText("Login");
                    loginTag.setText("");
                }
                mathetrainer.setMainMenuUpdate(false);
            }
        }));
    }
}