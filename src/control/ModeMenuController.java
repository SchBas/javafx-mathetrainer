package control;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import model.Main;
import model.Mathetrainer;

public class ModeMenuController implements ControllerInterface {

    Mathetrainer mathetrainer;

    @FXML
    public Button startButton;
    @FXML
    public Label modeMenuTitle;
    @FXML
    public Label modeMenuDescription;

    public void goToModeActive(ActionEvent actionEvent) {
        mathetrainer.setModeActiveScene();
    }

    public void goToChooseMode(ActionEvent actionEvent) {
        mathetrainer.setChooseModeScene();
    }

    public void goToHighscores(ActionEvent actionEvent) {
        mathetrainer.setHighscoresScene();
    }

    public void goToBestenliste(ActionEvent actionEvent) {
        mathetrainer.setBestenlisteScene();
    }

    public void setMathetrainer(Mathetrainer giveMathetrainer) {

        mathetrainer = giveMathetrainer;

        mathetrainer.modeMenuUpdate.addListener(((observableValue, aBoolean, t1) -> {
            if (mathetrainer.isModeMenuUpdate()) {
                startButton.requestFocus();
                modeMenuTitle.setText(mathetrainer.getCurrentMode().getModeTitle());
                modeMenuDescription.setText(mathetrainer.getCurrentMode().getModeDescription());
                mathetrainer.setModeMenuUpdate(false);
            }
        }));
    }
}