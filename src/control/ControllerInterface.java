package control;

import model.Mathetrainer;

public interface ControllerInterface {
    void setMathetrainer(Mathetrainer mathetrainer);
}