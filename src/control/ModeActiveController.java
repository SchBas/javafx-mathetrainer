package control;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import model.Main;
import model.Mathetrainer;

public class ModeActiveController implements ControllerInterface {

    @FXML
    public Label modeActiveTitle;
    public Label questionText;
    public TextField answerField;
    public Label lastTaskShowLabel;
    public Label lastTaskConfirmLabel;
    public Label timeLabel;
    public Label countFirstLabel;
    public Label countSecondLabel;

    Mathetrainer mathetrainer;

    public void goToModeMenu(ActionEvent actionEvent) {
        //mathetrainer.getCurrentMode().setModeStarted(false);
        //mathetrainer.setModeMenuScene();
        Main.subStage.setScene(Main.confirmModeCancelScene);
        Main.subStage.showAndWait();
    }

    public void submitAnswer(ActionEvent actionEvent) {
        if (!answerField.getText().equals("")) {
            mathetrainer.getCurrentMode().computeAnswer(answerField.getText());
            answerField.clear();
        }
        answerField.requestFocus();
    }

    public void setMathetrainer(Mathetrainer giveMathetrainer) {

        mathetrainer = giveMathetrainer;

        mathetrainer.modeActiveUpdate.addListener(((observableValue, aBoolean, t1) -> {
            if (mathetrainer.isModeActiveUpdate()) {
                if (!mathetrainer.getCurrentMode().isModeStarted()) {
                    System.out.println("Mode Start");
                    mathetrainer.getCurrentMode().modeStart();
                    answerField.requestFocus();
                    answerField.clear();
                    mathetrainer.getCurrentMode().setModeStarted(true);
                }
                updateLabels();
                mathetrainer.setModeActiveUpdate(false);
            }
        }));
    }

    public void updateLabels() {
        modeActiveTitle.setText(mathetrainer.getCurrentMode().getModeTitle());
        questionText.setText(mathetrainer.getCurrentMode().getActiveQuestion());
        lastTaskShowLabel.setText(mathetrainer.getCurrentMode().getLastTaskShowLabel());
        lastTaskConfirmLabel.setText(mathetrainer.getCurrentMode().getLastTaskConfirmLabel());
        timeLabel.setText(mathetrainer.getCurrentMode().getActiveTimeLabel());
        countFirstLabel.setText(mathetrainer.getCurrentMode().getActiveCountFirstLabel());
        countSecondLabel.setText(mathetrainer.getCurrentMode().getActiveCountSecondLabel());
    }
}