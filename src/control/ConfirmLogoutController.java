package control;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import model.Main;
import model.Mathetrainer;

public class ConfirmLogoutController implements ControllerInterface {

    public Button confirmButton;
    public Button cancelButton;
    Mathetrainer mathetrainer;

    public void goToMainMenu(ActionEvent actionEvent) {
        mathetrainer.setMainMenuScene();
    }

    public void setMathetrainer(Mathetrainer giveMathetrainer) {
        mathetrainer = giveMathetrainer;
    }

    public void confirm(ActionEvent actionEvent) {
        if (mathetrainer.computeLogout()) {
            Main.subStage.close();
            goToMainMenu(actionEvent);
        }
    }

    public void cancel(ActionEvent actionEvent) {
        Main.subStage.close();
    }
}
