package control;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Main;
import model.Mathetrainer;
import model.ScoreSet;

import java.io.IOException;

public class ModeHighscoresController implements ControllerInterface {

    @FXML
    public TableView<ScoreSet> highScoresTable;
    public int maxCells = 10;
    public Label modeHighscoresTitle;
    public TableColumn<ScoreSet, String> userColumn;
    public TableColumn<ScoreSet, Integer> column1;
    public TableColumn<ScoreSet, Integer> column2;
    public TableColumn<ScoreSet, Integer> pointsColumn;

    Mathetrainer mathetrainer;

    public void setMathetrainer(Mathetrainer giveMathetrainer) {
        mathetrainer = giveMathetrainer;

        userColumn.setCellValueFactory(new PropertyValueFactory<>("username"));
        pointsColumn.setCellValueFactory(new PropertyValueFactory<>("points"));

        mathetrainer.modeHighscoresUpdate.addListener(((observableValue, aBoolean, t1) -> {
            if (mathetrainer.isModeHighscoresUpdate()) {
                modeHighscoresTitle.setText(mathetrainer.getCurrentMode().getModeTitle() + " Highscores");

                for ( int i = 0; i < highScoresTable.getItems().size(); i++) {
                    highScoresTable.getItems().clear();
                }
                column1.setCellValueFactory(new PropertyValueFactory<>(mathetrainer.getCurrentMode().getFirstScoreColumn()));
                column1.setText(mathetrainer.getCurrentMode().getFirstScoreColumnName());
                column2.setCellValueFactory(new PropertyValueFactory<>(mathetrainer.getCurrentMode().getSecondScoreColumn()));
                column2.setText(mathetrainer.getCurrentMode().getSecondScoreColumnName());

                ScoreSet[] highScoreArray = new ScoreSet[0];
                try {
                    highScoreArray = mathetrainer.getHighScoreArraySecure();
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
                for (int cellCount = 0; cellCount < maxCells; cellCount++) {
                    if (highScoreArray[cellCount] != null) highScoresTable.getItems().add(highScoreArray[cellCount]);
                }

                mathetrainer.setModeHighscoresUpdate(false);
            }
        }));
    }

    public void goBack(ActionEvent actionEvent) {
        Main.primaryStage.setScene(Main.modeMenuScene);
    }
}