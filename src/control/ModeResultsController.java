package control;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import model.Mathetrainer;

public class ModeResultsController implements ControllerInterface {

    @FXML
    public Label resultsTitle;
    public Label resultsFirstLabel;
    public Label resultsSecondLabel;
    public Label resultsThirdLabel;
    public Label resultsFirstDescLabel;
    public Label resultsSecondDescLabel;
    public Label resultsThirdDescLabel;

    Mathetrainer mathetrainer;

    public void setMathetrainer(Mathetrainer giveMathetrainer) {
        mathetrainer = giveMathetrainer;

        mathetrainer.modeResultsUpdate.addListener(((observableValue, aBoolean, t1) -> {
            if (mathetrainer.isModeResultsUpdate()) {
                updateLabels();
                mathetrainer.setModeResultsUpdate(false);
            }
        }));
    }

    public void goToModeMenu(ActionEvent actionEvent) {
        mathetrainer.setModeMenuScene();
    }

    public void updateLabels() {
        resultsTitle.setText(mathetrainer.getCurrentMode().getResultsTitle());
        resultsFirstDescLabel.setText(mathetrainer.getCurrentMode().getFirstResultsDescLabel());
        resultsSecondDescLabel.setText(mathetrainer.getCurrentMode().getSecondResultsDescLabel());
        resultsThirdDescLabel.setText(mathetrainer.getCurrentMode().getThirdResultsDescLabel());
        resultsFirstLabel.setText(mathetrainer.getCurrentMode().getFirstResultsLabel());
        resultsSecondLabel.setText(mathetrainer.getCurrentMode().getSecondResultsLabel());
        resultsThirdLabel.setText(mathetrainer.getCurrentMode().getThirdResultsLabel());
    }
}