package control;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import model.Main;
import model.Mathetrainer;

public class ConfirmModeCancelController implements ControllerInterface {

    public Button confirmButton;
    public Button cancelButton;
    Mathetrainer mathetrainer;

    public void setMathetrainer(Mathetrainer giveMathetrainer) {
        mathetrainer = giveMathetrainer;
    }

    public void confirm(ActionEvent actionEvent) {
        Main.subStage.close();
        mathetrainer.getCurrentMode().setModeStarted(false);
        mathetrainer.setModeMenuScene();
    }

    public void cancel(ActionEvent actionEvent) {
        Main.subStage.close();
    }
}
