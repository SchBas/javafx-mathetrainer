package control;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import model.Main;
import model.Mathetrainer;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

public class LoginController implements ControllerInterface {

    @FXML
    public TextField nameField;
    public TextField passwordField;
    public Button confirmLoginButton;
    public Label validMessageLabel;

    Mathetrainer mathetrainer;

    public void goToMainMenu(ActionEvent actionEvent) {
        mathetrainer.setMainMenuScene();
        validMessageLabel.setText("");
        //validMessageLabel.
    }

    public void setMathetrainer(Mathetrainer giveMathetrainer) {
        mathetrainer = giveMathetrainer;

        validMessageLabel.setText("");
        mathetrainer.loginUpdate.addListener(((observableValue, aBoolean, t1) -> {
            if (mathetrainer.isLoginUpdate()) {
                validMessageLabel.setText(mathetrainer.loginValidAlert);
                passwordField.setText("");

                mathetrainer.setLoginUpdate(false);
            }
        }));
    }

    public void confirmLogin(ActionEvent actionEvent) throws InterruptedException, ExecutionException, IOException, URISyntaxException {
        boolean success = mathetrainer.computeLogin(nameField.getText(), passwordField.getText());
        if (success) goToMainMenu(actionEvent);
        else validMessageLabel.setText("Invalid Login Data!");
    }

    public void confirmRegister(ActionEvent actionEvent) throws InterruptedException, ExecutionException, IOException {
        if (nameField.getText().isBlank() || passwordField.getText().isBlank()) {
            validMessageLabel.setText("Required Data Missing!");
        } else if (!mathetrainer.validateLoginSecure(nameField.getText(), passwordField.getText())) {
            Main.subStage.setScene(Main.confirmPasswordScene);
            mathetrainer.setConfirmPasswordUpdate(true);
            mathetrainer.testUsername = nameField.getText();
            mathetrainer.testPassword = passwordField.getText();

            validMessageLabel.setText("");

            Main.subStage.showAndWait();
        } else validMessageLabel.setText("User already exists!");
    }

    public void validateName(ActionEvent actionEvent) {
        passwordField.requestFocus();
    }

    public void validatePassword(ActionEvent actionEvent) {
        confirmLoginButton.requestFocus();
    }
}