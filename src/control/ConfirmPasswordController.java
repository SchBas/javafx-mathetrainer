package control;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import model.Main;
import model.Mathetrainer;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class ConfirmPasswordController implements ControllerInterface {

    public Button confirmButton;
    public Button cancelButton;
    public PasswordField passwordFieldTwo;
    Mathetrainer mathetrainer;

    public void goToMainMenu(ActionEvent actionEvent) {
        mathetrainer.setMainMenuScene();
    }

    public void setMathetrainer(Mathetrainer giveMathetrainer) {
        mathetrainer = giveMathetrainer;

        mathetrainer.confirmPasswordUpdate.addListener(((observableValue, aBoolean, t1) -> {
            if (mathetrainer.isConfirmPasswordUpdate()) {
                //passwordFieldTwo.setText(mathetrainer.testPassword);

                mathetrainer.setConfirmPasswordUpdate(false);
            }
        }));
    }

    public void confirm(ActionEvent actionEvent) throws InterruptedException, ExecutionException, IOException {
        if (passwordFieldTwo.getText().equals(mathetrainer.testPassword)
                && mathetrainer.registerUserSecure(mathetrainer.testUsername, mathetrainer.testPassword)) {

            mathetrainer.loginValidAlert = "";
            mathetrainer.setLoginUpdate(true);
            Main.subStage.close();
            goToMainMenu(actionEvent);
        } else {
            mathetrainer.loginValidAlert = "Password does not match!";
            mathetrainer.setLoginUpdate(true);
            passwordFieldTwo.setText("");
            Main.subStage.close();
        }
    }

    public void cancel(ActionEvent actionEvent) {
        Main.subStage.close();
    }
}
