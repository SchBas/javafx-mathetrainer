package control;

import javafx.event.ActionEvent;
import model.Main;
import model.Mathetrainer;

public class ChooseModeController implements ControllerInterface {

    Mathetrainer mathetrainer;

    public void goToMainMenu(ActionEvent actionEvent) {
        Main.primaryStage.setScene(Main.mainMenuScene);
    }

    public void goToModeOneMenu(ActionEvent actionEvent) {
        mathetrainer.setMode(1);
    }

    public void goToModeTwoMenu(ActionEvent actionEvent) {
        mathetrainer.setMode(2);
    }

    public void goToModeThreeMenu(ActionEvent actionEvent) {
        mathetrainer.setMode(3);
    }

    public void setMathetrainer(Mathetrainer giveMathetrainer) {
        mathetrainer = giveMathetrainer;
    }
}
