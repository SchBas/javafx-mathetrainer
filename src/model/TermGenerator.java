package model;

public class TermGenerator {

    //Task attributes
    private String task;
    private String correctAnswer;

    //generates one of the four term types at random by choosing one of the methods below, fills the task attributes
    public void generateRandomTerm(){
        int currentValue = 0;
        switch (getRandomNum(1, 4)) {
            case 1 -> currentValue = generateAddition(currentValue);
            case 2 -> currentValue = generateSubtraction(currentValue);
            case 3 -> currentValue = generateMultiplication(currentValue);
            case 4 -> currentValue = generateDivision(currentValue);
        }
        task = task + " = ";
        setCorrectAnswer(currentValue);
    }

    //same as above, only with a specified number of factors greater than two
    public void generateLongTerm(int termLength) {
        //currentValue is the full result of the current term described in task
        int currentValue = 0;
        //brackets is true whenever the next added factor may require the addition of brackets around the existing factors
        boolean brackets = false;

        //total factors = termLength + 1
        for (int i = 1; i <= termLength; i++) {
            //taskNum (1...4) determines the computation of the next added factor
            int taskNum;
            //if currentValue is 0, meaning there aren't any factors yet, so any computation is allowed
            if (currentValue == 0) taskNum = getRandomNum(1, 4);
            //if currentValue is above 90, only subtractions are allowed (WIP)
            else if (currentValue > 90) taskNum = 2;
            //if currentValue is above 50, both additions and subtractions are allowed
            else if (currentValue > 50) taskNum = getRandomNum(1, 2);
            //if currentValue is below 10, additions, multiplications and divisions are allowed
            else if (currentValue < 10) {
                taskNum = getRandomNum(1, 3);
                if (taskNum > 1) taskNum++;
            //on the currentValue range between 10 and 50, all computations are allowed
            } else taskNum = getRandomNum(1, 4);

            switch (taskNum) {
                //Addition (outer brackets)
                case 1 -> {
                    currentValue = generateAddition(currentValue);
                    brackets = true;
                }
                //Subtraction (outer brackets)
                case 2 -> {
                    currentValue = generateSubtraction(currentValue);
                    brackets = true;
                }
                //Multiplication (inner brackets)
                case 3 -> {
                    if (brackets) task = "(" + task + ")";
                    currentValue = generateMultiplication(currentValue);
                    brackets = false;
                }
                //Division (inner and outer brackets)
                case 4 -> {
                    if (brackets) task = "(" + task + ")";
                    currentValue = generateDivision(currentValue);
                    brackets = true;
                }
            }
        }
        task = task + " = ";
        setCorrectAnswer(currentValue);
    }

    //generates a simple addition term, returns the answer as int
    public int generateAddition(int currentValue) {
        int smallFactor;
        //if there are no factors yet, generate the first factor
        if (currentValue == 0) {
            smallFactor = getRandomNum(2, 97);
            task = smallFactor + "";
            //otherwise the first factor becomes the result of all existing factors
        } else smallFactor = currentValue;
        //second factor is generated based on the first, to be greater
        int bigFactor = getRandomNum(smallFactor + 2, 100);
        //third factor is the difference between the first two
        int thirdFactor = (bigFactor - smallFactor);

        //the task can be displayed with both smaller factors switched (first factor can be a combination of several factors)
        switch (getRandomNum(1, 2)) {
            case 1 -> task = task + " + " + thirdFactor;
            case 2 -> task = thirdFactor + " + " + task;
        }
        return bigFactor;
    }

    //generates a simple subtraction term, returns the answer as int
    public int generateSubtraction(int currentValue) {
        int bigFactor;
        //if there are no factors yet, generate the first factor
        if (currentValue == 0) {
            bigFactor = getRandomNum(4, 100);
            task = bigFactor + "";
            //otherwise the first factor becomes the result of all existing factors
        } else bigFactor = currentValue;
        //second factor is generated based on the first, to be smaller
        int smallFactor = getRandomNum(2, bigFactor - 2);

        //the task is displayed and the difference between both factors is returned as the result
        task = task + " - " + smallFactor;
        return bigFactor - smallFactor;
    }

    //generates a simple multiplication term, returns the answer as int
    public int generateMultiplication(int currentValue) {
        int bigFactor;
        //if there are no factors yet, generate the first factor
        if (currentValue == 0) {
            bigFactor = getRandomNum(2, 50);
            task = bigFactor + "";
            //otherwise the first factor becomes the result of all existing factors
        } else bigFactor = currentValue;
        //second factor is generated based on the first, so that their product is 100 or less
        int smallFactor = getRandomNum(2, (int)Math.floor(100 / (float)bigFactor));

        //the task can be displayed with both smaller factors switched (first factor can be a combination of several factors)
        switch (getRandomNum(1, 2)) {
            case 1 -> task = task + " * " + smallFactor;
            case 2 -> task = smallFactor + " * " + task;
        }
        return bigFactor * smallFactor;
    }

    //generates a simple division term, returns the answer as int
    public int generateDivision(int currentValue) {
        int bigFactor;
        //if there are no factors yet, generate the first factor
        if (currentValue == 0) {
            bigFactor = getRandomNum(2, 50);
            task = bigFactor + "";
            //otherwise the first factor becomes the result of all existing factors
        } else bigFactor = currentValue;
        //second factor is generated based on the first, so that their product is 100 or less
        int smallFactor = getRandomNum(2, (int)Math.floor(100 / (float)bigFactor));
        //third factor is generated as the product of the other two factors
        int thirdFactor = (bigFactor * smallFactor);

        //the task is displayed and the quotient of both other factors is returned as the result
        task = thirdFactor + " / " + task;
        return smallFactor;
    }

    //returns a random number between the specified limits
    public int getRandomNum(int lowerLimit, int upperLimit) {
        return (int)Math.floor(Math.random() * (upperLimit-lowerLimit + 1) + lowerLimit);
    }

    //Getter & setter for task attributes
    private void setCorrectAnswer(int answer) {
        correctAnswer = Integer.toString(answer);
    }

    public String getTask() {
        return task;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }
}