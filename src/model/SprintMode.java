package model;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.util.Duration;

public class SprintMode extends AbstractMode {

    public SprintMode (BooleanProperty modeActiveUpdate, BooleanProperty modeEnd) {
        //passing on update parameters to the abstract class
        super(modeActiveUpdate, modeEnd);

        //Mode as a number
        modeNum = 2;

        //Text for the labels on the ModeMenu screen
        modeTitle = "Sprint Mode";
        modeDescription = "Answer as many questions as possible before time runs out.";
    }

    //The length of the term to be generated
    public int termLength = 3;

    //parameters relevant for the score
    public int answerCount = 0;
    public int rightAnswerCount = 0;
    public int timeLimit = 50;
    public int timeSeconds = 0;
    public int totalPoints = 0;

    //Timer initiation
    Timeline timeline = new Timeline(new KeyFrame(
            Duration.millis(1000),
            ae -> timerTick()
    ));

    //Sets the text for the labels to be displayed on the results screen
    public void setResultsText() {
        resultsTitle = "Sprint Mode Results";
        firstResultsDescLabel = "Total Answers:";
        firstResultsLabel = "" + answerCount;
        secondResultsDescLabel = "Right Answers:";
        secondResultsLabel = "" + rightAnswerCount;
        thirdResultsDescLabel = "Total Points:";
        thirdResultsLabel = "" + totalPoints;
    }

    //Returns the result of a round of training as an object
    public ScoreSet getScoreSet() {
        return new ScoreSet("", 2, answerCount, rightAnswerCount, timeLimit, totalPoints);
    }

    //Getter for the table columns in HighScores and Bestenliste
    public String getFirstScoreColumn() {
        return "totalAnswers";
    }

    public String getFirstScoreColumnName() {
        return "Total Answers";
    }

    public String getSecondScoreColumn() {
        return "rightAnswers";
    }

    public String getSecondScoreColumnName() {
        return "Right Answers";
    }

    //Setup at the beginning of the training + timer start
    public void modeStart() {
        //first task generated and displayed
        termGenerator.generateLongTerm(termLength);
        activeQuestion = termGenerator.getTask();

        //count of answered questions to 0
        answerCount = 0;
        rightAnswerCount = 0;
        activeCountFirstLabel = "Answered: (0)";
        activeCountSecondLabel = "Correct: (0)";

        //timer display to 0
        timeSeconds = timeLimit;
        activeTimeLabel = timeLimit + " seconds";

        //timer start
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    //Update with every second of the timer
    private void timerTick() {
        timeSeconds--;
        activeTimeLabel = timeSeconds + " seconds";
        //if timer has reached zero: stop the timer, count the score, set the text for the labels on the results screen and give mathetrainer an update signal
        if (timeSeconds == 0) {
            timeline.stop();

            if (rightAnswerCount == 0) totalPoints = 0;
            else totalPoints = (rightAnswerCount * 10 * ((rightAnswerCount * 100) / answerCount)) / 100;
            //P = R * 10 * R/RF

            setResultsText();
            setModeEnd(true);
        } else {
            if(isModeStarted()) setModeActiveUpdate(true);
        }
    }

    //Receive answer from ActiveController, check if it's correct and initiate next step
    public void computeAnswer(String answer) {
        answerCount++;
        lastTaskShowLabel = termGenerator.getTask() + answer;
        //If answer is correct: count right answer
        if (answer.equals(termGenerator.getCorrectAnswer())) {
            rightAnswerCount++;
            lastTaskConfirmLabel = "Correct";
        } else lastTaskConfirmLabel = "False";
        activeCountFirstLabel = "Answered: (" + answerCount + ")";
        activeCountSecondLabel = "Correct: (" + rightAnswerCount + ")";

        //Generate new question and update the controller
        termGenerator.generateLongTerm(termLength);
        activeQuestion = termGenerator.getTask();
        setModeActiveUpdate(true);
    }
}