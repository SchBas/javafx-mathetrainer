package model;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class User {

    //user attributes
    public String username;
    int userid;
    String password;

    //constructor to set attributes
    public User(String newUsername, String newPassword) throws IOException, InterruptedException, URISyntaxException {
        username = newUsername;
        password = newPassword;


        // create a client
        HttpClient client = HttpClient.newHttpClient();

        // create a request
        HttpRequest request = HttpRequest.newBuilder(
                URI.create("http://bastian-schmider-phaser3-test.atwebpages.com/mathetrainer/?action=getuserid&username=" + username + "&password=" + password)
        ).build();

        /*
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("http://bastian-schmider-phaser3-test.atwebpages.com/mathetrainer/"))
                .headers("action", "getuserid", "userid", "123")
                .GET()
                .build();
                */

        // use the client to send the request
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        // the response:
        System.out.println(response.statusCode());
        //System.out.println(response.body());

        try {
            userid = Integer.parseInt(response.body());
            System.out.println("userid: " + userid);
        } catch (Exception e) {
            System.out.println("userid is not int");
            userid = 1;
        }
    }
}