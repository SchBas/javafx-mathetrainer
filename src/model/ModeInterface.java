package model;

public interface ModeInterface {

    //Methods called to signify the start and end of training
    boolean isModeStarted();
    void setModeStarted(boolean modeStarted);

    //Get current mode as an int (1-3)
    int getModeNum();

    //Getters for label text
    String getModeTitle();
    String getModeDescription();
    String getActiveQuestion();
    String getLastTaskShowLabel();
    String getLastTaskConfirmLabel();
    String getActiveTimeLabel();
    String getActiveCountFirstLabel();
    String getActiveCountSecondLabel();
    String getResultsTitle();
    String getFirstResultsDescLabel();
    String getFirstResultsLabel();
    String getSecondResultsDescLabel();
    String getSecondResultsLabel();
    String getThirdResultsDescLabel();
    String getThirdResultsLabel();

    //Methods called from ActiveController
    void modeStart();
    void computeAnswer(String answer);

    //Getters for scores and score table data
    ScoreSet getScoreSet();
    String getFirstScoreColumn();
    String getFirstScoreColumnName();
    String getSecondScoreColumn();
    String getSecondScoreColumnName();
}