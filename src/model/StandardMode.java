package model;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.util.Duration;

public class StandardMode extends AbstractMode {

    public StandardMode (BooleanProperty modeActiveUpdate, BooleanProperty modeEnd) {
        //passing on update parameters to the abstract class
        super(modeActiveUpdate, modeEnd);

        //Mode as a number
        modeNum = 1;

        //Text for the labels on the ModeMenu screen
        modeTitle = "Standard Mode";
        modeDescription = "Answer a set of " + totalQuestions + " questions.";
    }

    //The length of the term to be generated
    public int termLength = 1;

    //parameters relevant for the score
    public int totalQuestions   = 10;
    public int answerCount      = 0;
    public int rightAnswerCount = 0;
    public int timeSeconds      = 0;
    public int totalPoints      = 0;

    //Timer initiation
    Timeline timeline = new Timeline(new KeyFrame(
            Duration.millis(1000),
            ae -> timerTick()
    ));

    //Sets the text for the labels to be displayed on the results screen
    public void setResultsText() {
        resultsTitle = "Standard Mode Results";
        firstResultsDescLabel = "Right Answers:";
        firstResultsLabel = rightAnswerCount + " / " + totalQuestions;
        secondResultsDescLabel = "Time spent:";
        secondResultsLabel = timeSeconds + " seconds";
        thirdResultsDescLabel = "Total Points:";
        thirdResultsLabel = "" + totalPoints;
    }

    //Returns the result of a round of training as an object
    public ScoreSet getScoreSet() {
        return new ScoreSet("", 1, answerCount, rightAnswerCount, timeSeconds, totalPoints);
    }

    //Getter for the table columns in HighScores and Bestenliste
    public String getFirstScoreColumn() {
        return "rightAnswers";
    }

    public String getFirstScoreColumnName() {
        return "Right Answers";
    }

    public String getSecondScoreColumn() {
        return "time";
    }

    public String getSecondScoreColumnName() {
        return "Seconds";
    }

    //Setup at the beginning of the training + timer start
    public void modeStart() {
        //first task generated and displayed
        termGenerator.generateLongTerm(termLength);
        activeQuestion = termGenerator.getTask();

        //count of answered questions to 0
        answerCount = 0;
        rightAnswerCount = 0;
        activeCountFirstLabel = "Answered: (0/" + totalQuestions + ")";
        activeCountSecondLabel = "Correct: (0)";

        //timer display to 0
        timeSeconds = 0;
        activeTimeLabel = "0 seconds";

        //timer start
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    //Update with every second of the timer
    private void timerTick() {
        timeSeconds++;
        activeTimeLabel = timeSeconds + " seconds";
        if(isModeStarted()) setModeActiveUpdate(true);
    }

    //Receive answer from ActiveController, check if it's correct and initiate next step
    public void computeAnswer(String answer) {
        answerCount++;
        lastTaskShowLabel = termGenerator.getTask() + answer;
        //If answer is correct: count right answer
        if (answer.equals(termGenerator.getCorrectAnswer())) {
            rightAnswerCount++;
            lastTaskConfirmLabel = "Correct";
        } else lastTaskConfirmLabel = "False";
        activeCountFirstLabel = "Answered: (" + answerCount + "/" + totalQuestions + ")";
        activeCountSecondLabel = "Correct: (" + rightAnswerCount + ")";
        //if max question count is reached: stop the timer, count the score, set the text for the labels on the results screen and give mathetrainer an update signal
        if (answerCount == totalQuestions) {
            timeline.stop();

            if (timeSeconds == 0) timeSeconds = 1;
            totalPoints = rightAnswerCount * rightAnswerCount * 100 / timeSeconds;
            //P = R * R * 100 / t

            setResultsText();
            setModeEnd(true);
            //If max question count is not yet reached: generate new question and update the controller
        } else {
            //termGenerator.generateRandomTerm();
            termGenerator.generateLongTerm(termLength);
            activeQuestion = termGenerator.getTask();
            setModeActiveUpdate(true);
        }
    }
}