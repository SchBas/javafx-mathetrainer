package model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;

public class Mathetrainer {

    //CONTROLLER-UPDATES

    //Getter and Setter for the observable BooleanProperties:

    public boolean isModeMenuUpdate() {
        return modeMenuUpdate.get();
    }

    public BooleanProperty modeMenuUpdateProperty() {
        return modeMenuUpdate;
    }

    public boolean isModeActiveUpdate() {
        return modeActiveUpdate.get();
    }

    public BooleanProperty modeActiveUpdateProperty() {
        return modeActiveUpdate;
    }

    public boolean isModeResultsUpdate() {
        return modeResultsUpdate.get();
    }

    public BooleanProperty modeResultsUpdateProperty() {
        return modeResultsUpdate;
    }

    public void setModeMenuUpdate(boolean modeMenuUpdate) {
        this.modeMenuUpdate.set(modeMenuUpdate);
    }

    public void setModeActiveUpdate(boolean modeActiveUpdate) {
        this.modeActiveUpdate.set(modeActiveUpdate);
    }

    public void setModeResultsUpdate(boolean modeResultsUpdate) {
        this.modeResultsUpdate.set(modeResultsUpdate);
    }

    public boolean isMainMenuUpdate() {
        return mainMenuUpdate.get();
    }

    public BooleanProperty mainMenuUpdateProperty() {
        return mainMenuUpdate;
    }

    public void setMainMenuUpdate(boolean mainMenuUpdate) {
        this.mainMenuUpdate.set(mainMenuUpdate);
    }

    public boolean isLoginUpdate() {
        return loginUpdate.get();
    }

    public BooleanProperty loginUpdateProperty() {
        return loginUpdate;
    }

    public void setLoginUpdate(boolean loginUpdate) {
        this.loginUpdate.set(loginUpdate);
    }

    public boolean isModeHighscoresUpdate() {
        return modeHighscoresUpdate.get();
    }

    public BooleanProperty modeHighscoresUpdateProperty() {
        return modeHighscoresUpdate;
    }

    public void setModeHighscoresUpdate(boolean update) {
        this.modeHighscoresUpdate.set(update);
    }

    public boolean isModeBestenlisteUpdate() {
        return modeBestenlisteUpdate.get();
    }

    public BooleanProperty modeBestenlisteUpdateProperty() {
        return modeBestenlisteUpdate;
    }

    public void setModeBestenlisteUpdate(boolean modeBestenlisteUpdate) {
        this.modeBestenlisteUpdate.set(modeBestenlisteUpdate);
    }

    public boolean isConfirmPasswordUpdate() {
        return confirmPasswordUpdate.get();
    }

    public BooleanProperty confirmPasswordUpdateProperty() {
        return confirmPasswordUpdate;
    }

    public void setConfirmPasswordUpdate(boolean confirmPasswordUpdate) {
        this.confirmPasswordUpdate.set(confirmPasswordUpdate);
    }

    public boolean isModeEnd() {
        return modeEnd.get();
    }

    public BooleanProperty modeEndProperty() {
        return modeEnd;
    }

    public void setModeEnd(boolean modeEnd) {
        this.modeEnd.set(modeEnd);
    }

    //Declaration and Initialization of the observable BooleanProperties:

    public BooleanProperty mainMenuUpdate = new SimpleBooleanProperty();
    public BooleanProperty loginUpdate = new SimpleBooleanProperty();
    public BooleanProperty modeMenuUpdate = new SimpleBooleanProperty();
    public BooleanProperty modeActiveUpdate = new SimpleBooleanProperty();
    public BooleanProperty modeResultsUpdate = new SimpleBooleanProperty();
    public BooleanProperty modeHighscoresUpdate = new SimpleBooleanProperty();
    public BooleanProperty modeBestenlisteUpdate = new SimpleBooleanProperty();

    public BooleanProperty confirmPasswordUpdate = new SimpleBooleanProperty();

    public BooleanProperty modeEnd = new SimpleBooleanProperty();

    //SET-SCENE METHODS

    //Go to specific Scenes, with prior updates and other modifiers
    public void setMainMenuScene() {
        setMainMenuUpdate(true);
        Main.primaryStage.setScene(Main.mainMenuScene);
    }

    public void setChooseModeScene() {
        Main.primaryStage.setScene(Main.chooseModeScene);
    }

    public void setModeMenuScene() {
        setModeMenuUpdate(true);
        Main.primaryStage.setScene(Main.modeMenuScene);
    }

    public void setModeActiveScene() {
        setModeActiveUpdate(true);
        Main.primaryStage.setScene(Main.modeActiveScene);
    }

    public void setModeResultsScene() {
        setModeResultsUpdate(true);
        Main.primaryStage.setScene(Main.modeResultsScene);
        getCurrentMode().setModeStarted(false);
    }

    public void setHighscoresScene() {
        setModeHighscoresUpdate(true);
        Main.primaryStage.setScene(Main.modeHighscoresScene);
    }

    public void setBestenlisteScene() {
        setModeBestenlisteUpdate(true);
        Main.primaryStage.setScene(Main.modeBestenlisteScene);
    }

    //SETUP MODE-MODELS

    //Initialization of objects and interface from Mode-Models
    StandardMode standardMode = new StandardMode(modeActiveUpdate, modeEnd);
    SprintMode sprintMode     = new SprintMode(modeActiveUpdate, modeEnd);
    PerfectMode perfectMode   = new PerfectMode(modeActiveUpdate, modeEnd);
    ModeInterface currentMode = standardMode;

    //Getter for the interface -> links to everything inside the models
    public ModeInterface getCurrentMode() {
        return currentMode;
    }

    //Assign model to interface based on chosen mode, go to Mode-Menu
    public void setMode(int mode) {

        if      (mode == 1) currentMode = standardMode;
        else if (mode == 2) currentMode = sprintMode;
        else if (mode == 3) currentMode = perfectMode;

        setModeMenuScene();
    }


    public boolean validateLoginSecure(String name, String password) throws IOException, InterruptedException, ExecutionException {

        HTTPRequestClass reqClass = new HTTPRequestClass();
        String uriString = "http://bastian-schmider-phaser3-test.atwebpages.com/mathetrainer/?action=validatelogin&username=" + name
                + "&password=" + password;

        int checkCount = 0;
        try {
            checkCount = Integer.parseInt(reqClass.stringRequest(uriString));
            System.out.println("Count: " + checkCount);
            if (checkCount == 1) System.out.println("user already exists");
            else System.out.println("user does not exist yet");
        } catch (Exception e) {
            System.out.println("checkCount is not int");
        }

        return (checkCount == 1);
    }

    public boolean registerUserSecure(String username, String password) throws InterruptedException, ExecutionException, IOException {
        if (username.isBlank() || password.isBlank() ) {
            System.out.println("registration form is blank");
            return false;
        } else if (!validateLoginSecure(username, password)) {

            HTTPRequestClass reqClass = new HTTPRequestClass();
            String uriString = "http://bastian-schmider-phaser3-test.atwebpages.com/mathetrainer/?action=registeruser&username="
                    + username + "&password=" + password;
            reqClass.simpleRequest(uriString);

            System.out.println("user registered");
            return true;

        } else return false;
    }

    //LOGIN

    //Login Test-Data
    public String testUsername;
    public String testPassword;

    public void setLoginTestData(String username, String password) {
        testUsername = username;
        testPassword = password;
    }

    public String[] getLoginTestData() {
        return new String[]{testUsername, testPassword};
    }

    //relevant attributes
    public String loginValidAlert = "";
    public boolean loggedIn = false;
    public User user;

    //Handles the login, initiated by the Login-button
    public boolean computeLogin(String username, String password) throws InterruptedException, ExecutionException, IOException, URISyntaxException {
        if (username.isBlank() || password.isBlank() ) {
            System.out.println("login is blank");
            return false;
        } else if (!validateLoginSecure(username, password)) {
            System.out.println("login is invalid");
            return false;
        } else {
            user = new User(username, password);
            loggedIn = true;
            return true;
        }
    }

    //Handles the logout, initiated by the Logout-button
    public boolean computeLogout() {
        user = null;
        loggedIn = false;
        return true;
    }



    //Constructor with a Listener:

    public Mathetrainer() {
        modeEnd.addListener(((observableValue, aBoolean, t1) -> {
            //If a round of training has ended: get results as ScoreSet-object, go to results screen
            if (isModeEnd()) {
                if (user == null) addArrayScoreSet();
                else try {
                    addScoreToDBSecure();
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
                setModeResultsScene();
                setModeEnd(false);
            }
        }));
    }

    //COLLECTING SCORES

    //Intermediate collection in scoreArray (to be replaced by a database later)
    public ScoreSet[] offlineScoreArray = new ScoreSet[50];
    private int scoreIndex = 0;

    //Extracts the ScoreSet object from the mode-model and adds it to the array
    public void addArrayScoreSet() {
        offlineScoreArray[scoreIndex] = currentMode.getScoreSet();
        offlineScoreArray[scoreIndex].username = "User";
        scoreIndex++;
        if (scoreIndex > offlineScoreArray.length) scoreIndex = 0;
    }

    public boolean addScoreToDBSecure() throws IOException, InterruptedException {
        ScoreSet newSet = currentMode.getScoreSet();
        int userid = user.userid, mode = newSet.mode, totalanswers = newSet.totalAnswers, rightanswers = newSet.rightAnswers,
                time = newSet.time, points = newSet.points;

        HTTPRequestClass reqClass = new HTTPRequestClass();
        String uriString = "http://bastian-schmider-phaser3-test.atwebpages.com/mathetrainer/?action=addscore&userid="
                + userid + "&mode=" + mode + "&totalanswers=" + totalanswers + "&rightanswers=" + rightanswers + "&time="
                + time + "&points=" + points;
        reqClass.simpleRequest(uriString);

        System.out.println("userid: " + userid + ", mode: " + mode + ", points: " + points);

        return true;
    }

    public ScoreSet[] getHighScoreArraySecure() throws IOException, InterruptedException {
        ScoreSet[] highScoreArray = new ScoreSet[20];
        if (user == null) {

            int pointsBelow = Integer.MAX_VALUE;
            int arrayItemCount = 0;
            for (ScoreSet arrayItem : offlineScoreArray) if (arrayItem != null && arrayItemCount <= 20) arrayItemCount++;

            for (int highI = 0; highI < arrayItemCount; highI++) {
                int currentMaxPoints = 0;
                int targetI = 0;
                for (int arrayI = 0; arrayI < arrayItemCount; arrayI++) {
                    if (offlineScoreArray[arrayI].mode == getCurrentMode().getModeNum() && offlineScoreArray[arrayI].points > currentMaxPoints
                            && offlineScoreArray[arrayI].points < pointsBelow) {
                        targetI = arrayI;
                        currentMaxPoints = offlineScoreArray[arrayI].points;
                    }
                }
                highScoreArray[highI] = offlineScoreArray[targetI];
                pointsBelow = highScoreArray[highI].points;
            }
        } else {

            String uriString = "http://bastian-schmider-phaser3-test.atwebpages.com/mathetrainer/?action=gethighscores&userid="
                    + user.userid + "&mode=" + getCurrentMode().getModeNum();

            highScoreArray = getOnlineScoreArray(uriString);
        }

        return highScoreArray;
    }

    public ScoreSet[] getBestenlisteArraySecure() throws IOException, InterruptedException {

        String uriString = "http://bastian-schmider-phaser3-test.atwebpages.com/mathetrainer/?action=getbestenliste&mode=" + getCurrentMode().getModeNum();

        return getOnlineScoreArray(uriString);
    }

    public ScoreSet[] getOnlineScoreArray(String uriString) throws IOException, InterruptedException {
        ScoreSet[] scoreArray = new ScoreSet[20];

        HTTPRequestClass reqClass = new HTTPRequestClass();

        String[] scoreContentArray = reqClass.stringRequest(uriString).split(" ");
        //wenn der zurückgegebene String leer ist, dann ist scoreContentArray.length == 1
        int bestI = 0;
        for (int i = 0; i < scoreContentArray.length && scoreContentArray.length != 1; i = i+6) {
            try {
                scoreArray[bestI] = new ScoreSet(
                        scoreContentArray[i],
                        Integer.parseInt(scoreContentArray[i+1]),
                        Integer.parseInt(scoreContentArray[i+2]),
                        Integer.parseInt(scoreContentArray[i+3]),
                        Integer.parseInt(scoreContentArray[i+4]),
                        Integer.parseInt(scoreContentArray[i+5])
                );
                bestI++;
            } catch (Exception e) {
                e.printStackTrace();
                e.getCause();
            }
        }

        return scoreArray;
    }
}