package model;

import control.ControllerInterface;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Main extends Application {

    //Test8

    //public attributes for the stage and the scenes to be set for that stage, accessible from any other class
    public static Stage primaryStage;
    public static Stage subStage = new Stage();
    public static Scene mainMenuScene, loginScene, chooseModeScene, modeMenuScene, modeActiveScene, modeResultsScene,
            modeHighscoresScene, modeBestenlisteScene, confirmPasswordScene, confirmLogoutScene, confirmExitScene,
            confirmModeCancelScene;

    //mathetrainer-object that is handed over to every controller-object as the connecting logical unit
    private final Mathetrainer mathetrainer = new Mathetrainer();

    //JavaFX-method to initiate the interface
    @Override
    public void start(Stage primaryStage) throws Exception {

        //Remember:
        //Parent mainMenuRoot = FXMLLoader.load(getClass().getResource("mainMenu.fxml"));
        //modeActiveScene = new Scene(FXMLLoader.load(getClass().getResource("modeActive.fxml")));
        /*
        FXMLLoader mainMenuLoader = new FXMLLoader(getClass().getResource("mainMenu.fxml"));
        Parent mainMenuRoot = mainMenuLoader.load();
        MainMenuController mainMenuController = mainMenuLoader.getController();
        mainMenuController.setGeneralModel(mathetrainer);
        mainMenuScene = new Scene(mainMenuRoot);
        */

        //all scenes are being initiated via the custom initiateScene method
        mainMenuScene = new Scene(initiateScene("view/mainMenuNew.fxml"));

        loginScene = new Scene(initiateScene("view/loginNew.fxml"));

        chooseModeScene = new Scene(initiateScene("view/chooseModeNew.fxml"));

        modeMenuScene = new Scene(initiateScene("view/modeMenuNew.fxml"));

        modeActiveScene = new Scene(initiateScene("view/modeActiveNew.fxml"));

        modeResultsScene = new Scene(initiateScene("view/modeResultsNew.fxml"));

        modeHighscoresScene = new Scene(initiateScene("view/modeHighscoresNew.fxml"));

        modeBestenlisteScene = new Scene(initiateScene("view/modeBestenlisteNew.fxml"));

        confirmPasswordScene = new Scene(initiateScene("view/confirmPassword.fxml"));

        confirmLogoutScene = new Scene(initiateScene("view/confirmLogout.fxml"));

        confirmExitScene = new Scene(initiateScene("view/confirmExit.fxml"));

        confirmModeCancelScene = new Scene(initiateScene("view/confirmModeCancel.fxml"));


        //set primarystage, display mainMenuScene on the stage
        Main.primaryStage = primaryStage;
        primaryStage.setTitle("Mathetrainer");
        primaryStage.setScene(mainMenuScene);

        //set the subStage to block out the primaryStage when shown
        Main.subStage.initModality(Modality.WINDOW_MODAL);
        Main.subStage.initOwner(primaryStage);

        //show the stage as a window
        primaryStage.show();
    }

    private Parent initiateScene(String resource) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../" + resource));
        Parent root = loader.load();
        ControllerInterface controllerInterface = loader.getController();
        controllerInterface.setMathetrainer(mathetrainer);
        return root;
    }

    public static void main(String[] args) {

        launch(args);
    }
}