package model;

import javafx.beans.property.BooleanProperty;

public abstract class AbstractMode implements ModeInterface {

    //Update trigger setters
    public void setModeActiveUpdate(boolean modeActiveUpdate) {
        this.modeActiveUpdate.set(modeActiveUpdate);
    }

    public void setModeEnd(boolean modeEnd) {
        this.modeEnd.set(modeEnd);
    }

    //Update triggers (BooleanProperty)
    public BooleanProperty modeActiveUpdate;
    public BooleanProperty modeEnd;

    //Constructor to set the update triggers
    public AbstractMode(BooleanProperty modeActiveUpdate, BooleanProperty modeEnd) {
        this.modeActiveUpdate  = modeActiveUpdate;
        this.modeEnd = modeEnd;
    }

    //Object used to generate terms and get related text and numbers
    TermGenerator termGenerator = new TermGenerator();

    //Describes whether training is currently occurring (important for certain updates)
    boolean modeStarted = false;

    //modeStarted getter/setter
    public boolean isModeStarted() {
        return modeStarted;
    }

    public void setModeStarted(boolean modeStarted) {
        this.modeStarted = modeStarted;
    }

    //modeNum getter
    public int getModeNum() {
        return modeNum;
    }

    //describes the current mode as a number
    int modeNum = 0;
    //String attributes containing text for the varying labels in the mode views
    String modeTitle          = "";
    String modeDescription    = "";
    String activeQuestion     = "";
    String lastTaskShowLabel  = "";
    String lastTaskConfirmLabel = "";
    String activeTimeLabel    = "";
    String activeCountFirstLabel = "";
    String activeCountSecondLabel = "";
    String resultsTitle       = "";
    String firstResultsDescLabel = "";
    String firstResultsLabel  = "";
    String secondResultsDescLabel = "";
    String secondResultsLabel = "";
    String thirdResultsDescLabel = "";
    String thirdResultsLabel  = "";

    public String getModeTitle() {
        return modeTitle;
    }

    public String getModeDescription() {
        return modeDescription;
    }

    public String getActiveQuestion() {
        return activeQuestion;
    }

    public String getLastTaskShowLabel() {
        return lastTaskShowLabel;
    }

    public String getLastTaskConfirmLabel() {
        return lastTaskConfirmLabel;
    }

    public String getActiveTimeLabel() {
        return activeTimeLabel;
    }

    public String getActiveCountFirstLabel() {
        return activeCountFirstLabel;
    }

    public String getActiveCountSecondLabel() {
        return activeCountSecondLabel;
    }

    public String getResultsTitle() {
        return resultsTitle;
    }

    @Override
    public String getFirstResultsDescLabel() {
        return firstResultsDescLabel;
    }

    @Override
    public String getSecondResultsDescLabel() {
        return secondResultsDescLabel;
    }

    @Override
    public String getThirdResultsDescLabel() {
        return thirdResultsDescLabel;
    }

    public String getFirstResultsLabel() {
        return firstResultsLabel;
    }

    public String getSecondResultsLabel() {
        return secondResultsLabel;
    }

    public String getThirdResultsLabel() {
        return thirdResultsLabel;
    }
}
