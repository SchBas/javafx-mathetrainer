package model;

public class ScoreSet {

    //Attribute getters
    public String getUsername() {
        return username;
    }

    public int getMode() {
        return mode;
    }

    public int getTotalAnswers() {
        return totalAnswers;
    }

    public int getRightAnswers() {
        return rightAnswers;
    }

    public int getTime() {
        return time;
    }

    public int getPoints() {
        return points;
    }

    //Attributes
    public String username;
    public int mode;
    public int totalAnswers;
    public int rightAnswers;
    public int time;
    public int points;

    //Constructor with attribute fillings
    public ScoreSet(String username, int mode, int totalAnswers, int rightAnswers, int time, int points) {
        this.username = username;
        this.mode = mode;
        this.totalAnswers = totalAnswers;
        this.rightAnswers = rightAnswers;
        this.time = time;
        this.points = points;
    }
}
